<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['pinterest'] = [
		'key' => '5819c158f9f4f',
		'name' => 'pinterest',
		'label' => 'Pinterest',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c158f9f50',
				'label' => 'Pinterest',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'https://pinterest.com/',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 8, 1);
