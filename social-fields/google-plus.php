<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['google_plus'] = [
		'key' => '5819c19bf9f56',
		'name' => 'google_plus',
		'label' => 'Google Plus',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c19bf9f57',
				'label' => 'Google Plus',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'https://plus.google.com/',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 7, 1);
