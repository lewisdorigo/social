<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['web'] = [
		'key' => '5819c200f9f62',
		'name' => 'web',
		'label' => 'Custom Link',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c20df9f63',
				'label' => 'Custom',
				'name' => 'link',
				'type' => 'url',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 21, 1);
