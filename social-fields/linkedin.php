<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['linkedin'] = [
		'key' => '5819c13cf9f4d',
		'name' => 'linkedin',
		'label' => 'LinkedIn',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c13cf9f4e',
				'label' => 'LinkedIn',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'https://linkedin.com/',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 6, 1);
