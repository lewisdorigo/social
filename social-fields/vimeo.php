<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['vimeo'] = [
		'key' => '5819c1daf9f5e',
		'name' => 'vimeo',
		'label' => 'Vimeo',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c1daf9f5f',
				'label' => 'Vimeo',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'https://vimeo.com/',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 16, 1);
