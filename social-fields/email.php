<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['email'] = [
		'key' => '5819c1e8f9f60',
		'name' => 'email',
		'label' => 'Email',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c1e8f9f61',
				'label' => 'Email',
				'name' => 'link',
				'type' => 'email',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 20, 1);
