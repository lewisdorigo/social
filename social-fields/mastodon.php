<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['mastodon'] = [
		'key' => '5819c200f9f62a',
		'name' => 'mastodon',
		'label' => 'Mastodon',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c20df9f63b',
				'label' => 'Mastodon',
				'name' => 'link',
				'type' => 'text',
				'instructions' => 'This should include your federation <br>e.g. @dorigo@mastodon.social',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 50,
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 4, 1);
