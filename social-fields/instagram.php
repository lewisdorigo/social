<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['instagram'] = [
		'key' => '5819c17cf9f51',
		'name' => 'instagram',
		'label' => 'Instagram',
		'display' => 'row',
		'sub_fields' => array (
		    array (
		    	'key' => 'field_5819c17cf9f52',
		    	'label' => 'Instagram',
		    	'name' => 'link',
		    	'type' => 'text',
		    	'instructions' => '',
		    	'required' => 1,
		    	'conditional_logic' => 0,
		    	'wrapper' => array (
		    		'width' => '',
		    		'class' => '',
		    		'id' => '',
		    	),
		    	'default_value' => '',
		    	'placeholder' => '',
		    	'prepend' => 'https://instagram.com/',
		    	'append' => '',
		    	'maxlength' => '',
		    ),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 5, 1);
