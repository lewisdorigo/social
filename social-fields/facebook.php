<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['facebook'] = [
		'key' => '5819c11cf9f4b',
		'name' => 'facebook',
		'label' => 'Facebook',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c129f9f4c',
				'label' => 'Facebook',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'https://facebook.com/',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 3, 1);
