<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['youtube'] = [
		'key' => '5819c1bcf9f5a',
		'name' => 'youtube',
		'label' => 'YouTube',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c1bcf9f5b',
				'label' => 'YouTube',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'https://www.youtube.com/channel/',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 15, 1);
