<?php

add_filter('Brave/CustomFields/SocialLinks', function($networks) {
   	$networks['twitter'] = [
		'key' => '5819c0ae200b6',
		'name' => 'twitter',
		'label' => 'Twitter',
		'display' => 'row',
		'sub_fields' => array (
			array (
				'key' => 'field_5819c0fcf9f4a',
				'label' => 'Twitter',
				'name' => 'link',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '@',
				'append' => '',
				'maxlength' => '',
			),
		),
		'min' => '',
		'max' => '1',
    ];

   return $networks;
}, 2, 1);
