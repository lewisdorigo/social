<?php
/**
 * Plugin Name:       Social
 * Plugin URI:        https://bitbucket.org/madebrave/social
 * Description:
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */


if ( !defined( 'ABSPATH' ) ) {
	die();
}

if(file_exists(__DIR__.'/vendor')) {
    include __DIR__.'/vendor/autoload.php';
}

if(!function_exists("get_field")) {
    add_action('admin_notices', function() {
    	$class = 'notice notice-error';
    	$message = __('The Thumbnails plugin requires Advanced Custom Fields to be enabled.');

    	printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    });
} else {

    acf_add_options_sub_page([
        'page_title'     => 'Social Accounts',
        'menu_title'     => 'Social Accounts',
        'parent'         => 'options-general.php',
        'capability'	 => 'switch_themes',
        'slug'           => 'drgo-social-accounts'
    ]);

    require_once __DIR__.'/fields.php';

    require_once __DIR__.'/classes/SocialAccount.php';
    require_once __DIR__.'/classes/SocialPost.php';
    require_once __DIR__.'/classes/SocialShare.php';

    require_once __DIR__.'/lib/Accounts.php';
    require_once __DIR__.'/lib/Sharing.php';

}