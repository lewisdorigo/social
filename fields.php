<?php use Brave\File;

if( function_exists('acf_add_local_field_group') ):

$directory = __DIR__.'/social-fields';

foreach(scandir($directory) as $file) {
    $path = $directory.DIRECTORY_SEPARATOR.$file;
    $ext = pathinfo($path, PATHINFO_EXTENSION);

    if(strpos($file, '.') === 0) {
        continue;
    } else if(!is_dir($path) && is_file($path) && $ext === "php") {
        require_once $path;
    }
}

unset($directory, $path, $ext, $file);

acf_add_local_field_group(array (
	'key' => 'group_57c594449769b',
	'title' => 'Social Links',
	'fields' => array (
		array (
			'key' => 'field_5819c072f9f49',
			'label' => 'Social Accounts',
			'name' => 'social_accounts',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Social Account',
			'min' => '',
			'max' => '',
			'layouts' => apply_filters('Brave/CustomFields/SocialLinks', []),
		),
	),
	'location' => apply_filters('Brave/CustomFields/SocialLinks/Location', array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'drgo-social-accounts',
			),
		),
	)),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
