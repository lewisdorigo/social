<?php namespace Dorigo\Social;

class Sharing {
    private $post;
    private $socialPost;
    private $sociaAccounts;

    private $availableNetworks = [
        "facebook"  => "getFacebookShare",
        "twitter"   => "getTwitterShare",
        "linkedin"  => "getLinkedInShare",
        "pinterest" => "getPinterestShare",
        "email"     => "getEmailShare"
    ];

    public function __construct($post_id = null) {
        $this->post = get_post($post_id);

        $socialPost = new \StdClass();

        $socialPost->link    = get_the_permalink($this->post->ID);
        $socialPost->title   = get_the_title($this->post->ID);
        $socialPost->summary = get_the_excerpt($this->post->ID);

        $this->socialPost = apply_filters("Dorigo/Social/Sharing/Post", $socialPost, $this->post->ID);

        unset($socialPost);
    }

    public function get($networks = ['facebook', 'twitter']) {
        return $this->getLinks($networks);
    }

    public function getLinks($networks = ['facebook', 'twitter']) {
        $return = [];

        foreach($networks as $network) {
            $network = preg_replace("/\s/","-", strtolower($network));
            $data = null;

            if(array_key_exists($network, $this->availableNetworks)) {
                $data = $this->{$this->availableNetworks[$network]}();
            }

            $data = apply_filters("Dorigo/Social/Sharing/network={$network}", $data, $network, $this->socialPost, $this->post);
            $data = apply_filters("Dorigo/Social/Sharing", $data, $network, $this->socialPost, $this->post);

            if(!is_null($data)) {
                $return[$network] = $data;
            }
        }

        unset($data);
        return $return;
    }

    private function getFacebookShare() {
        $query = [
            "app_id"  => apply_filters("Dorigo/Social/Sharing/FacebookAppID", null),
            "display" => "popup",
            "href"    => $this->socialPost->link,
            "quote"   => $this->socialPost->summary,
            "hashtag" => apply_filters("Dorigo/Social/Sharing/Facebook/Hashtag", null, $this->post),
        ];

        return new SocialShare([
            "network" => "facebook",
            "title"   => "Facebook",
            "link"    => "https://www.facebook.com/dialog/share?".http_build_query($query),
        ]);
    }

    private function getTwitterShare() {
        $query = [
            "text"    => $this->socialPost->title,
            "url"     => $this->socialPost->link,
            "related" => apply_filters("Dorigo/Social/Sharing/Twitter/Related", null, $this->post),
        ];

        return new SocialShare([
            "network" => "twitter",
            "title"   => "Twitter",
            "link"    => "https://twitter.com/intent/tweet?".http_build_query($query),
        ]);
    }

    private function getLinkedInShare() {
        $query = [
            "url"    => $this->socialPost->link,
            "title"  => $this->socialPost->title,
            "mini"   => "true",
            "source" => apply_filters("Dorigo/Social/Sharing/LinkedIn/Source", get_bloginfo("name"))
        ];

        return new SocialShare([
            "network" => "linkedin",
            "title"   => "LinkedIn",
            "link"    => "https://www.linkedin.com/shareArticle?".http_build_query($query),
        ]);
    }

    private function getEmailShare() {
        $body = $this->socialPost->title.PHP_EOL.($this->socialPost->summary?$this->socialPost->summary.PHP_EOL:"").PHP_EOL."Read more at: {$this->socialPost->link}";

        $query = [
            "subject" => apply_filters("Dorigo/Social/Sharing/Email/Subject", $this->socialPost->title." on ".get_bloginfo("name"), $this->socialPost, $this->post),
            "body"    => apply_filters("Dorigo/Social/Sharing/Email/Subject", $body, $this->socialPost, $this->post),
        ];

        return new SocialShare([
            "network" => "email",
            "title"   => "Email",
            "link"    => "mailto:?".http_build_query($query),
        ]);
    }

    private function getPinterestShare() {
        $query = [
            "url"         => $this->socialPost->link,
            "description" => $this->socialPost->title
        ];

        return new SocialShare([
            "network" => "pinterest",
            "title"   => "Pinterest",
            "link"    => "https://www.pinterest.com/pin/create/button/?".http_build_query($query),
        ]);
    }
}
