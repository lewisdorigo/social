<?php namespace Dorigo\Social;

use Dorigo\Singleton\Singleton;
use Dorigo\Social\SocialAccount;

class Accounts extends Singleton {
    private $accounts;

    private $defaultFormats = [
        'twitter' => [
            'format' => 'https://twitter.com/{{link}}',
            'name'   => 'Twitter',
            'icon'   => 'social__twitter',
            'prefix' => '@',
        ],
        'facebook' => [
            'format' => 'https://facebook.com/{{link}}',
            'name'   => 'Facebook',
            'icon'   => 'social__facebook',
            'prefix' => '',
        ],
        'linkedin' => [
            'format' => 'https://linkedin.com/{{link}}',
            'name'   => 'LinkedIn',
            'icon'   => 'social__linkedin',
            'prefix' => '',
        ],
        'instagram' => [
            'format' => 'https://instagram.com/{{link}}',
            'name'   => 'Instagram',
            'icon'   => 'social__instagram',
            'prefix' => '',
        ],
        'pinterest' => [
            'format' => 'https://pinterest.com/{{link}}',
            'name'   => 'Pinterest',
            'icon'   => 'social__pinterest',
            'prefix' => '',
        ],
        'google-plus' => [
            'format' => 'https://plus.google.com/{{link}}',
            'name'   => 'Google Plus',
            'icon'   => 'social__google-plus',
            'prefix' => '+',
        ],
        'youtube' => [
            'format' => 'https://youtube.com/channel/{{link}}',
            'name'   => 'YouTube',
            'icon'   => 'social__youtube',
            'prefix' => '',
        ],
        'vimeo' => [
            'format' => 'https://youtube.com/{{link}}',
            'name'   => 'Vimeo',
            'icon'   => 'social__vimeo',
            'prefix' => '',
        ],
        'email' => [
            'format' => '{{link}}',
            'name'   => 'Email Us',
            'icon'   => 'social__email',
            'prefix' => '',
        ],
        'mastodon' => [
            'format' => '{{link}}',
            'name'   => 'Mastodon',
            'icon'   => 'social__mastodon',
            'prefix' => '',
        ],
        'custom' => [
            'format' => '{{link}}',
            'name'   => 'Website',
            'icon'   => 'social__web',
            'prefix' => '',
        ],
    ];

    protected function __construct() {
        add_filter('social_post', [$this,'encodeTitle'], 10, 1);

        add_filter('Dorigo/Social/Account/network=mastodon', [$this,'mastodonLink'], 10, 2);
    }

    public function accounts() {
        if(!is_null($this->accounts)) { return $this->accounts; }

        $this->accounts = [];

        if(!get_field('social_accounts','options')) { return []; }

        while(have_rows('social_accounts','options')) {
            the_row();

            $network = get_row_layout();
            $link = get_sub_field('link');

            $this->accounts[$network] = $this->formatLink($network, $link);
        }

        $this->accounts = array_filter($this->accounts);
        return $this->accounts;
    }

    public function mastodonLink($data, $link) {
        $link = ltrim($link, '@');
        $link = explode('@', $link);

        $link = array_values(array_filter($link));

        if(count($link) != 2) { return $data; }

        $data->set('link', "https://{$link[1]}/@{$link[0]}/");

        return $data;
    }

    public function formatLink($network, $link) {
        $link = apply_filters("Dorigo/Social/Account/BeforeFormat/network={$network}", $link, $network);
        $link = apply_filters("Dorigo/Social/Account/BeforeFormat", $link, $network);

        $networks = apply_filters("Dorigo/Social/FormatLink/Formats", $this->defaultFormats);

        $network = in_array($network,['web','link'])?'custom':$network;

        if(!array_key_exists($network, $networks)) { return false; }

        $data = $networks[$network];
        $data = new SocialAccount($data);

        if($network === 'custom') {
            $data->set('name', parse_url($link, PHP_URL_HOST));
        }

        $data->set('raw', $link);
        $data->set('text', $data->get("prefix", "").$link);
        $data->set('link', str_replace('{{link}}', $link, $data->format));

        $data = apply_filters("Dorigo/Social/Account/network={$network}", $data, $link, $network);
        return  apply_filters("Dorigo/Social/Account", $data, $link, $network);
    }
}

Accounts::getInstance();
