<?php namespace Dorigo\Social;

class SocialAccount extends \StdClass {
    public $name;
    public $format;
    public $icon;
    public $prefix;
    public $link;

    public function __construct(array $titles = []) {
        foreach($titles as $type => $content) {
            $this->{$type} = $content;
        }
    }

    public function set(string $type, string $content) {
        $this->{$type} = $content;
    }

    public function get(string $type, $default = null) {
        return isset($this->{$type}) ? $this->{$type} : $default;
    }
}