<?php namespace Dorigo\Social;

class SocialShare extends \StdClass {
    public $title;
    public $network;
    public $link;

    public function __construct(array $titles = []) {
        foreach($titles as $type => $content) {
            $this->{$type} = $content;
        }
    }

    public function set(string $type, string $content) {
        $this->{$type} = $content;
    }

    public function get(string $type, $default = null) {
        return isset($this->{$type}) ? $this->{$type} : $default;
    }

    public function __toString() {
        return '<a href="'.$this->link.'" target="_blank" class="social-share social-share--'.$this->network.'>'.$this->name.'</a>';
    }
}